const chai = require('chai');
const assert = chai.assert;

const fs = require('fs');
const testDir = 'src';

fs.readdir(testDir, (err, files) =>
{
    if (err)
    {
        throw err;
    }

    describe('test-parse', function()
    {
        for (let i = 0; i < files.length; ++i)
        {
            it(files[i], (filename = files[i]) =>
            {
                try
                {
                    JSON.parse(fs.readFileSync(`${testDir}/${filename}`, 'utf8'));
                    assert.isTrue(true);
                }
                catch (e)
                {
                    throw new Error(e.message);
                }
            });
        }
    });
});
